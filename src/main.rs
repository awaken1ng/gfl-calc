#![allow(dead_code)]
#![allow(unused_variables)]

mod doll;
mod equipment;
mod fairy;
mod formation_buff;
mod formation;

use doll::Doll;
use fairy::Fairy;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    id: u32,
    
    #[structopt(short, long, default_value = "100")]
    level: u16,

    #[structopt(short, long, default_value = "0.0")]
    affection: f32,

    #[structopt(short, long)]
    is_night: bool,
}

fn main() {
    let opt = Opt::from_args();
    println!("{:?}", opt);

    let dolls = doll::load_gun_data("5005_Gun.csv", "gun.json").unwrap();
 
    let base = dolls.into_iter().find(|b| b.id == opt.id).unwrap(); 
    println!("{:?}", base);

    let doll = Doll::new_with_level(&base, opt.level.into(), opt.affection);
    println!("{}", doll);
    println!("CE: {:?}", doll.ce(opt.is_night, true, false));
}

// fn max_stats_base_dolls(dolls: &Vec<&DollBase>, is_oathed: bool) -> Vec<(u32, String, Doll)> {
//     dolls
//         .iter()
//         .map(|base| {
//             let favour_ratio = if is_oathed {
//                 if base.id > 20000 { 0.15 } else { 0.10 }
//             } else {
//                 0.05
//             };
//             (base.id, base.name.clone(), Doll::new_with_max_stats(base, favour_ratio))
//         })
//         .collect()
// }
//
// fn max_stats_best_equip_dolls(dolls: &Vec<&DollBase>, equips: &Vec<Equipment>, is_oathed: bool, is_night: bool, is_full_hp: bool, with_tilebuffs: bool) -> Vec<(u32, String, Doll)> {
//     max_stats_base_dolls(dolls, is_oathed)
//         .into_iter()
//         .map(|(doll_id, name, mut doll)| {
//             let mut best_ce = 0.0;
//             let mut best_equip = (None, None, None);
// 
//             macro_rules! check_slot {
//                 ($slot:tt) => {{
//                     let (category, types) = &doll.compatible_equipment.$slot;
//                     for equip in equips.iter() {
//                         if &equip.category != category { continue; }
//                         if !types.contains(&equip.r#type) { continue; }
//                         if !equip.fits.is_empty() { if !equip.fits.contains(&doll_id) { continue; } }
//                        
//                         doll.equipment.$slot = Some(equip.clone());
//                         let ce = doll.ce(is_night, is_full_hp, with_tilebuffs);
//                         if ce > best_ce {
//                             best_ce = ce;
//                             best_equip.$slot = Some(equip);
//                         }
//                     }
//                     doll.equipment.$slot = best_equip.$slot.map(Clone::clone);
//                 }};
//             }
//
//             check_slot!(0);
//             check_slot!(1);
//             check_slot!(2);
//             // recheck slots in case we can get higher CE with current equipment
//             check_slot!(0);
//             check_slot!(1);
//             check_slot!(2);
//
//             (doll_id, name, doll)
//         })
//         .collect()
// }
//
// fn best_fairy_for_doll(doll: &mut Doll, fairies: &Vec<Fairy>, is_night: bool, is_full_hp: bool, with_tilebuffs: bool) -> f32 {
//     let mut best_ce = 0.0;
//     let mut best_fairy = None;
//     for fairy in fairies.iter() {
//         doll.fairy = Some(fairy.clone());
//         let ce = doll.ce(is_night, is_full_hp, with_tilebuffs);
//         if ce > best_ce {
//             best_ce = ce;
//             best_fairy = Some(fairy);
//         }
//     }
//     doll.fairy = best_fairy.map(Clone::clone);
//     best_ce
// }
