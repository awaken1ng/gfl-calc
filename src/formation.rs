use crate::{Doll, Fairy};

pub struct Formation {
    pub fairy: Option<Fairy>,
    pub dolls: [Option<(i8, Doll)>; 5], // (position, doll)
}

impl Formation {
    pub fn empty() -> Self {
        Self { fairy: None, dolls: [None, None, None, None, None] }
    }

    fn set_doll(&mut self, slot: usize, position: i8, doll: Doll) {
        self.dolls[slot] = Some((position, doll))
    }

    pub fn ce(&mut self, is_night: bool, is_full_hp: bool) -> f32 {
        let dolls: Vec<(i8, Doll)> = self.dolls
            .clone()
            .to_vec()
            .into_iter()
            .filter(|s| s.is_some())
            .map(|s| s.unwrap())
            .collect()
            ;

        dolls.into_iter()
            .map(|(pos, mut doll)| {
                self.dolls
                    .iter()
                    .for_each(|d2| {
                        if let Some((p2, d2)) = d2 {
                            if d2.formation_buff.tiles.hit_test(*p2, pos) {
                                doll.tile_buffs = d2.formation_buff.effects.clone();
                            }
                        }
                    });

                (pos, doll)
            })
            .fold(0.0, |mut total, (_, mut doll)| {
                doll.fairy = self.fairy.clone();
                total += doll.ce(is_night, is_full_hp, true);
                total
            })
    }
}