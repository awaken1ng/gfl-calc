use serde::{Deserialize, Deserializer, de};
use std::collections::HashMap;
use std::fmt::Display;
use std::fs;
use std::path::Path;
use std::str::FromStr;
use std::fmt;

const FP_GROWTH: [f32; 4] = [7.0, 0.076, 100.0, 100.0];
const EVA_GROWTH: [f32; 4] = [20.0, 0.202, 100.0, 100.0];
const ACC_GROWTH: [f32; 4] = [25.0, 0.252, 100.0, 100.0];
const ARMOUR_GROWTH: [f32; 4] = [5.0, 0.05, 100.0, 100.0];
const CRIT_DMG_GROWTH: [f32;4] = [10.0, 0.101, 100.0, 100.0];

#[derive(Debug, Deserialize)]
pub struct FairyBase {
    #[serde(deserialize_with = "as_maybe_str")]
    id: u32,
    name: String,
    #[serde(rename = "pow")]
    #[serde(deserialize_with = "as_maybe_str")]
    firepower: f32,
    #[serde(rename = "hit")]
    #[serde(deserialize_with = "as_maybe_str")]
    accuracy: f32,
    #[serde(rename = "dodge")]
    #[serde(deserialize_with = "as_maybe_str")]
    evasion: f32,
    #[serde(rename = "armor")]
    #[serde(deserialize_with = "as_maybe_str")]
    armour: f32,
    #[serde(rename = "critical_harm_rate")]
    #[serde(deserialize_with = "as_maybe_str")]
    crit_dmg: f32,
    #[serde(rename = "grow")]
    #[serde(deserialize_with = "as_maybe_str")]
    growth_factor: f32,
}

#[derive(Debug, Clone)]
pub struct Fairy {
    pub id: u32,
    pub name: String,
    pub firepower: f32,
    pub accuracy: f32,
    pub evasion: f32,
    pub armour: f32,
    pub crit_dmg: f32
}

impl FairyBase {
    pub fn with_level(&self, level: u16) -> Fairy {
        let level: f32 = level.into();

        macro_rules! calculate {
            ($name:ident, $base:expr, $growth:expr) => {
                let $name = f32::ceil($growth[0] * $base / $growth[2]) + f32::ceil((level - 1.0) * $growth[1] * $base / $growth[2] * self.growth_factor / $growth[3]);
            };
        }

        calculate!(firepower, self.firepower, FP_GROWTH);
        calculate!(accuracy, self.accuracy, ACC_GROWTH);
        calculate!(evasion, self.evasion, EVA_GROWTH);
        calculate!(armour, self.armour, ARMOUR_GROWTH);
        calculate!(crit_dmg, self.crit_dmg, CRIT_DMG_GROWTH);

        Fairy {
            id: self.id,
            name: self.name.clone(),
            firepower,
            accuracy,
            evasion,
            armour,
            crit_dmg
        }
    }
}

pub fn load_fairy_data<P>(catch: P, text: P) -> Vec<FairyBase>
where
    P: AsRef<Path>,
{
    let text: HashMap<String, String> = {
        let contents: Vec<u8> = fs::read(text).expect("failed to read text data");
        serde_json::from_slice(&contents).expect("failed to deserialize text data")
    };
    let fairies: Vec<FairyBase> = {
        let contents: Vec<u8> = fs::read(catch).expect("failed to read catch data");
        serde_json::from_slice(&contents).expect("failed to deserialize catch data")
    };

    // filter out player unusable fairies
    // replace name with actual name instead of text id
    let fairies: Vec<FairyBase> = fairies.into_iter().filter_map(|mut base| {
        if base.id > 90000 {
            None
        } else {
            base.name = text.get(&base.name).unwrap().to_owned();
            Some(base)
        }
    }).collect();

    fairies
}

pub fn as_maybe_str<'de, D, V>(deserializer: D) -> Result<V, D::Error>
where
    D: Deserializer<'de>,
    V: FromStr + Deserialize<'de>,
    V::Err: Display,
{
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum MaybeStr<V> {
        String(String),
        Value(V),
    }

    match MaybeStr::deserialize(deserializer)? {
        MaybeStr::String(s) => Ok(V::from_str(&s).map_err(de::Error::custom)?),
        MaybeStr::Value(v) => Ok(v),
    }
}

impl fmt::Display for Fairy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Fairy {{ name: {}", self.name
        )?;
        if self.firepower != 0.0 { write!(f, ", fp: {}%", self.firepower)? }
        if self.accuracy != 0.0 { write!(f, ", acc: {}%", self.accuracy)? }
        if self.evasion != 0.0 { write!(f, ", eva: {}%", self.evasion)? }
        if self.crit_dmg != 0.0 { write!(f, ", crit dmg: {}%", self.crit_dmg)? }
        if self.armour != 0.0 { write!(f, ", armour: {}%", self.armour)? }
        write!(f, " }}")
    }
}
