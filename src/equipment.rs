use std::collections::HashMap;
use std::fmt;
use std::path::Path;
use Category::*;
use Type::*;

#[derive(Debug, PartialEq, Clone)]
pub enum Category {
    Accessory,
    Ammo,
    BodyGear,
}

impl Category {
    pub fn from_u8(val: u8) -> Option<Self> {
        match val {
            1 => Some(Accessory),
            2 => Some(Ammo),
            3 => Some(BodyGear),
            _ => None,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Type {
    OpticalSight,
    HolographicSight,
    RedDotSight,
    NightVisionDevice,
    Suppressor,

    AP,
    HP,
    ShotgunShells,
    HV,

    Chip,
    Exoskeleton,
    Armour,
    AmmoBox,
    CamouflageCloak,

    SPEQ,
}

impl Type {
    pub fn from_u8(val: u8) -> Option<Self> {
        match val {
            1 => Some(OpticalSight),
            2 => Some(HolographicSight),
            3 => Some(RedDotSight),
            4 => Some(NightVisionDevice),
            13 => Some(Suppressor),

            5 => Some(AP),
            6 => Some(HP),
            7 => Some(ShotgunShells),
            8 => Some(HV),

            9 => Some(Chip),
            10 => Some(Exoskeleton),
            11 => Some(Armour),
            14 => Some(AmmoBox),
            15 => Some(CamouflageCloak),

            12 | 16 | 17 => Some(SPEQ),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Equipment {
    pub id: u32,
    pub name: String,
    pub rarity: u8,
    pub category: Category,
    pub r#type: Type,
    pub firepower: f32,
    pub accuracy: f32,
    pub evasion: f32,
    pub rof: f32,
    pub crit_dmg: f32,
    pub crit_rate: f32,
    pub armour_pierce: f32,
    pub armour: f32,
    pub night_vision: f32,
    pub ammo_rounds: f32,
    pub skill_effect: f32,
    pub fits: Vec<u32>,
}

impl Equipment {
    pub fn from_stc(record: &HashMap<String, String>, text: &HashMap<String, String>) -> anyhow::Result<Self> {
        let name = {
            let key = record.get("name").unwrap();
            text.get(key)
                .filter(|v| v.is_empty())
                .unwrap_or(record.get("code").unwrap())
        };
        let category = {
            let val = record.get("category").unwrap().parse()?;
            Category::from_u8(val).unwrap()
        };
        let r#type = {
            let val = record.get("type").unwrap().parse()?;
            Type::from_u8(val).unwrap()
        };

        let mut stats: HashMap<String, f32> = HashMap::new();
        macro_rules! fetch_into_hashmap {
            ($key:expr) => {{
                let value = record
                    .get($key)
                    .unwrap()
                    .split(',')
                    .last()
                    .unwrap()
                    .parse()
                    .unwrap_or_default();
                stats.insert($key.into(), value);
            }};
        }
        fetch_into_hashmap!("pow");
        fetch_into_hashmap!("hit");
        fetch_into_hashmap!("dodge");
        fetch_into_hashmap!("rate");
        fetch_into_hashmap!("critical_harm_rate");
        fetch_into_hashmap!("critical_percent");
        fetch_into_hashmap!("armor_piercing");
        fetch_into_hashmap!("armor");
        fetch_into_hashmap!("night_view_percent");
        fetch_into_hashmap!("bullet_number_up");
        fetch_into_hashmap!("skill_effect_per");

        let enhancement_bonus = record.get("bonus_type").unwrap();
        for bonus in enhancement_bonus.split(",") {
            if bonus.is_empty() {
                continue;
            }
            let mut bonus = bonus.splitn(2, ":");
            let key = bonus.next().unwrap().to_string();
            let enhancement: f32 = bonus.next().unwrap().parse()?;

            let value = stats.get(&key).unwrap().clone();
            let max_value = f32::floor(value + (value * enhancement / 1000.0));
            stats.insert(key, max_value);
        }

        let fits = {
            let fit_guns = record.get("fit_guns").unwrap();
            if fit_guns.is_empty() {
                vec![]
            } else {
                fit_guns
                    .split(',')
                    .map(|i| i.parse().expect("failed to convert doll id in fit_guns"))
                    .collect()
            }
        };

        Ok(Self {
            id: record.get("id").unwrap().parse().unwrap(),
            name: name.to_owned(),
            rarity: record.get("rank").unwrap().parse().unwrap(),
            category,
            r#type,
            firepower: stats.remove("pow").unwrap(),
            accuracy: stats.remove("hit").unwrap(),
            evasion: stats.remove("dodge").unwrap(),
            rof: stats.remove("rate").unwrap(),
            crit_rate: stats.remove("critical_percent").unwrap(),
            crit_dmg: stats.remove("critical_harm_rate").unwrap(),
            armour_pierce: stats.remove("armor_piercing").unwrap(),
            armour: stats.remove("armor").unwrap(),
            night_vision: stats.remove("night_view_percent").unwrap(),
            ammo_rounds: stats.remove("bullet_number_up").unwrap(),
            skill_effect: stats.remove("skill_effect_per").unwrap(),
            fits,
        })
    }
}

pub fn load_equipment_data<P>(stc: P, text: P) -> anyhow::Result<Vec<Equipment>>
where
    P: AsRef<Path>,
{
    let text: HashMap<String, String> = {
        let contents = std::fs::read(text).unwrap();
        serde_json::from_slice(&contents).unwrap()
    };

    let equips = csv::Reader::from_path(stc)
        .expect("failed to open csv parser")
        .deserialize()
        .skip(1)
        .filter_map(Result::ok)
        .filter(|record: &HashMap<String, String>| {
            let rarity: u8 = record.get("rank").unwrap().parse().unwrap();
            rarity > 4
        })
        .map(|record| Equipment::from_stc(&record, &text).expect("failed to deserialize equipment record"))
        .collect();

    Ok(equips)
}

impl fmt::Display for Equipment {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Equipment {{ name: {}, rarity: {}*, category: {:?}, type: {:?}",
            self.name, self.rarity, self.category, self.r#type
        )?;
        if self.firepower != 0.0 {
            write!(f, ", fp: {}", self.firepower)?
        }
        if self.accuracy != 0.0 {
            write!(f, ", acc: {}", self.accuracy)?
        }
        if self.evasion != 0.0 {
            write!(f, ", eva: {}", self.evasion)?
        }
        if self.rof != 0.0 {
            write!(f, ", rof: {}", self.rof)?
        }
        if self.crit_dmg != 0.0 {
            write!(f, ", crit dmg: {}%", self.crit_dmg)?
        }
        if self.crit_rate != 0.0 {
            write!(f, ", crit rate: {}%", self.crit_rate)?
        }
        if self.armour_pierce != 0.0 {
            write!(f, ", ap: {}", self.armour_pierce)?
        }
        if self.armour != 0.0 {
            write!(f, ", armour: {}", self.armour)?
        }
        if self.night_vision != 0.0 {
            write!(f, ", night vision: {}%", self.night_vision)?
        }
        if self.ammo_rounds != 0.0 {
            write!(f, ", ammo: {}", self.ammo_rounds)?
        }
        if self.skill_effect != 0.0 {
            write!(f, ", skill: {}%", self.skill_effect)?
        }
        if !self.fits.is_empty() {
            write!(f, ", fits: {:?}", self.fits)?
        }
        write!(f, " }}")
    }
}
