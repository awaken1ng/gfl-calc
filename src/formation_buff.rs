use crate::doll;

#[derive(Debug, Clone, Default)]
pub struct FormationBuff {
    pub tiles: FormationBuffTiles,
    pub affects: Vec<doll::Type>, // if empty, buffs any type
    pub effects: FormationBuffEffects,
}

#[derive(Debug, Default, Clone)]
pub struct FormationBuffEffects {
    pub firepower: f32,
    pub rof: f32,
    pub accuracy: f32,
    pub evasion: f32,
    pub crit_rate: f32,
    pub cdr: f32,
    // pub armour_pierce: f32, // unused
    pub armour: f32,
}

#[derive(Debug, Clone)]
pub struct FormationBuffTiles {
    // numpad notation
    pub center: i8,
    pub affected: Vec<i8>,
}

impl Default for FormationBuffTiles {
    fn default() -> Self {
        Self { center: 5, affected: Vec::default() }
    }
}

impl FormationBuffTiles {
    pub fn new_from_stc(center: i8, affected: Vec<i8>) -> Self {
        // translate to numpad notation
        let shift = center - 13;
        Self {
            center: tile_center_to_numpad(center - (shift * 2)),
            affected: affected
                .into_iter()
                .map(|t| tile_center_to_numpad(t - shift))
                .collect()
        }
    }

    pub fn hit_test(&self, center: i8, target: i8) -> bool {
        if center == self.center {
            self.affected.contains(&target)
        } else {
            self.affected
                .iter()
                .filter_map(|tile| {
                    let source = numpad_to_xy(self.center);
                    let target = numpad_to_xy(center);
                    let delta = (target.0 - source.0, target.1 - source.1);
                    let tile = numpad_to_xy(*tile);
                    let recentered_x = tile.0 + delta.0;
                    if recentered_x == 0 || recentered_x > 3 { return None }
                    let recentered_y = tile.1 + delta.1;
                    if recentered_y == 0 || recentered_y > 3 { return None }
                    Some(xy_to_numpad((recentered_x, recentered_y)))
                })
                .find(|t| t == &target)
                .is_some()
        }
    }
}

fn tile_center_to_numpad(center: i8) -> i8 {
    match center {
        7 => 1,
        8 => 4,
        9 => 7,
       12 => 2,
       13 => 5,
       14 => 8,
       17 => 3,
       18 => 6,
       19 => 9,
       _ => panic!()
    }
}

fn numpad_to_xy(pos: i8) -> (i8, i8) {
    match pos {
        1 => (1, 1),
        2 => (1, 2),
        3 => (1, 3),
        4 => (2, 1),
        5 => (2, 2),
        6 => (2, 3),
        7 => (3, 1),
        8 => (3, 2),
        9 => (3, 3),
        _ => panic!()
    }
}

fn xy_to_numpad(xy: (i8, i8)) -> i8 {
    match xy {
        (1, 1) => 1,
        (1, 2) => 2,
        (1, 3) => 3,
        (2, 1) => 4,
        (2, 2) => 5,
        (2, 3) => 6,
        (3, 1) => 7,
        (3, 2) => 8,
        (3, 3) => 9,
        _ => panic!()
    }
}

#[test]
fn test() {
    // - □ -
    // □ ◯ □
    // - □ -
    let grid = FormationBuffTiles::new_from_stc(13, vec![8,12,14,18]);
    assert_eq!(grid.center, 5);
    assert_eq!(grid.affected, vec![4, 2, 8, 6]);

    // ◯ □ -
    // □ - -
    // - - -
    {
        let center = 7;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), true);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), true);
        assert_eq!(grid.hit_test(center, 5), false);
        assert_eq!(grid.hit_test(center, 6), false);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), false);
        assert_eq!(grid.hit_test(center, 3), false);
    }
    

    // □ ◯ □
    // - □ -
    // - - -
    {
        let center = 8;
        assert_eq!(grid.hit_test(center, 7), true);
        assert_eq!(grid.hit_test(center, 8), false);
        assert_eq!(grid.hit_test(center, 9), true);
        assert_eq!(grid.hit_test(center, 4), false);
        assert_eq!(grid.hit_test(center, 5), true);
        assert_eq!(grid.hit_test(center, 6), false);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), false);
        assert_eq!(grid.hit_test(center, 3), false);
    }

    // - □ ◯
    // - - □
    // - - -
    {
        let center = 9;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), true);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), false);
        assert_eq!(grid.hit_test(center, 5), false);
        assert_eq!(grid.hit_test(center, 6), true);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), false);
        assert_eq!(grid.hit_test(center, 3), false);
    }

    // □ - -
    // ◯ □ -
    // □ - -
    {
        let center = 4;
        assert_eq!(grid.hit_test(center, 7), true);
        assert_eq!(grid.hit_test(center, 8), false);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), false);
        assert_eq!(grid.hit_test(center, 5), true);
        assert_eq!(grid.hit_test(center, 6), false);
        assert_eq!(grid.hit_test(center, 1), true);
        assert_eq!(grid.hit_test(center, 2), false);
        assert_eq!(grid.hit_test(center, 3), false);
    }

    // - □ -
    // □ ◯ □
    // - □ -
    {
        let center = 5;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), true);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), true);
        assert_eq!(grid.hit_test(center, 5), false);
        assert_eq!(grid.hit_test(center, 6), true);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), true);
        assert_eq!(grid.hit_test(center, 3), false);
    }

    // - - □
    // - □ ◯
    // - - □
    {
        let center = 6;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), false);
        assert_eq!(grid.hit_test(center, 9), true);
        assert_eq!(grid.hit_test(center, 4), false);
        assert_eq!(grid.hit_test(center, 5), true);
        assert_eq!(grid.hit_test(center, 6), false);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), false);
        assert_eq!(grid.hit_test(center, 3), true);
    }

    // - - -
    // □ - -
    // ◯ □ -
    {
        let center = 1;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), false);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), true);
        assert_eq!(grid.hit_test(center, 5), false);
        assert_eq!(grid.hit_test(center, 6), false);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), true);
        assert_eq!(grid.hit_test(center, 3), false);
    }

    // - - -
    // - □ -
    // □ ◯ □
    {
        let center = 2;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), false);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), false);
        assert_eq!(grid.hit_test(center, 5), true);
        assert_eq!(grid.hit_test(center, 6), false);
        assert_eq!(grid.hit_test(center, 1), true);
        assert_eq!(grid.hit_test(center, 2), false);
        assert_eq!(grid.hit_test(center, 3), true);
    }

    // - - -
    // - - □
    // - □ ◯
    {
        let center = 3;
        assert_eq!(grid.hit_test(center, 7), false);
        assert_eq!(grid.hit_test(center, 8), false);
        assert_eq!(grid.hit_test(center, 9), false);
        assert_eq!(grid.hit_test(center, 4), false);
        assert_eq!(grid.hit_test(center, 5), false);
        assert_eq!(grid.hit_test(center, 6), true);
        assert_eq!(grid.hit_test(center, 1), false);
        assert_eq!(grid.hit_test(center, 2), true);
        assert_eq!(grid.hit_test(center, 3), false);
    }
}
