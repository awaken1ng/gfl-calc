use anyhow::Context;
use std::collections::HashMap;
use std::path::Path;
use std::fs;
use std::fmt;
use crate::equipment::{self, Equipment};
use crate::fairy::Fairy;
use crate::formation_buff::{FormationBuff, FormationBuffTiles, FormationBuffEffects};

const HP_BASE: [f32; 3] = [55.0, 0.555, 100.0];
const HP_BASE_MOD: [f32; 3] = [96.283, 0.138, 100.0];

const FP_BASE: [f32; 2] = [16.0, 100.0];
const FP_GROWTH: [f32; 4] = [0.242, 100.0, 100.0, 0.0];
const FP_GROWTH_MOD: [f32; 4] = [0.06, 100.0, 100.0, 18.018];

const ROF_BASE: [f32; 2] = [45.0, 100.0];
const ROF_GROWTH: [f32; 4] = [0.181, 100.0, 100.0, 0.0];
const ROF_GROWTH_MOD: [f32; 4] = [0.022, 100.0, 100.0, 15.741];

const ARMOUR_BASE: [f32; 3] = [2.0, 0.161, 100.0];
const ARMOUR_BASE_MOD: [f32; 3] = [13.979, 0.04, 100.0];

const EVA_BASE: [f32; 2] = [5.0, 100.0];
const EVA_GROWTH: [f32; 4] = [0.303, 100.0, 100.0, 0.0];
const EVA_GROWTH_MOD: [f32; 4] = [0.075, 100.0, 100.0, 22.572];

const ACC_BASE: [f32; 2] = [5.0, 100.0];
const ACC_GROWTH: [f32; 4] = [0.303, 100.0, 100.0, 0.0];
const ACC_GROWTH_MOD: [f32; 4] = [0.075, 100.0, 100.0, 22.572];

#[derive(Debug, Clone, Copy, Default)]
pub struct Stat {
    pub base: f32,
    pub additional: f32,
    pub affection: f32,
    pub equipment: f32,
    pub fairy: f32,
}

impl Stat {
    fn is_zero(&self) -> bool {
        self.base == 0.0 
            && self.additional == 0.0
            && self.affection == 0.0
            && self.equipment == 0.0
            && self.fairy == 0.0
    }

    fn total(&self) -> f32 {
        f32::ceil(self.base) 
            + f32::ceil(self.additional) 
            + f32::ceil(self.affection) 
            + self.equipment 
            + self.fairy
    }
}

impl fmt::Display for Stat {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let total = self.total();
        write!(f, "{}", total)?;

        if total != 0.0 {
            write!(f, " (")?;

            let mut v = vec![];
            if self.base       > 0.0 { v.push(format!("base: {}", self.base)) }
            if self.additional > 0.0 { v.push(format!("additional: {}", self.additional)) }
            if self.affection  > 0.0 { v.push(format!("affection: {}", self.affection)) }
            if self.equipment  > 0.0 { v.push(format!("equipment: {}", self.equipment)) }
            if self.fairy      > 0.0 { v.push(format!("fairy: {}", self.fairy)) }
            write!(f, "{}", v.join(", "))?;

            write!(f, ")")?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Type {
    HG,
    SMG,
    RF,
    AR,
    MG,
    SG,
}

use Type::*;

impl Type {
    pub fn from_u8(val: u8) -> Option<Self> {
        match val {
            1 => Some(HG),
            2 => Some(SMG),
            3 => Some(RF),
            4 => Some(AR),
            5 => Some(MG),
            6 => Some(SG),
            _ => None,
        }
    }

    pub const fn base_hp_factor(&self) -> f32 {
        match self {
            HG => 0.60,
            SMG => 1.60,
            RF => 0.80,
            AR => 1.00,
            MG => 1.50,
            SG => 2.00,
        }
    }

    pub const fn base_fp_factor(&self) -> f32 {
        match self {
            HG => 0.60,
            SMG => 0.60,
            RF => 2.40,
            AR => 1.00,
            MG => 1.80,
            SG => 0.70,
        }
    }

    pub const fn base_rof_factor(&self) -> f32 {
        match self {
            HG => 0.80,
            SMG => 1.20,
            RF => 0.50,
            AR => 1.00,
            MG => 1.60,
            SG => 0.40,
        }
    }

    pub const fn base_accuracy_factor(&self) -> f32 {
        match self {
            HG => 1.20,
            SMG => 0.30,
            RF => 1.60,
            AR => 1.00,
            MG => 0.60,
            SG => 0.30,
        }
    }

    pub const fn base_evasion_factor(&self) -> f32 {
        match self {
            HG => 1.80,
            SMG => 1.60,
            RF => 0.80,
            AR => 1.00,
            MG => 0.60,
            SG => 0.30,
        }
    }

    pub const fn base_armour_factor(&self) -> f32 {
        match self {
            HG => 0.00,
            SMG => 0.00,
            RF => 0.00,
            AR => 0.00,
            MG => 0.00,
            SG => 1.00,
        }
    }
}

#[derive(Debug, Clone)]
pub struct DollBase {
    pub id: u32,
    pub name: String,
    pub r#type: Type,
    pub rarity: u8, // 2-6
    pub crit_rate: u16,
    pub armour_pierce: u16,
    pub rounds: u16, // ammo stat for MGs and SGs
    pub growth_ratio: u16,
    pub stat_growth: StatGrowthRatio,
    pub formation_buff: FormationBuff,
    pub compatible_equipment: ((equipment::Category, Vec<equipment::Type>), (equipment::Category, Vec<equipment::Type>), (equipment::Category, Vec<equipment::Type>)),
}

#[derive(Debug, Clone)]
pub struct StatGrowthRatio {
    pub hp: f32,
    pub fp: f32,
    pub rof: f32,
    pub accuracy: f32,
    pub evasion: f32,
    pub armour: f32,
}

impl DollBase {
    fn from_stc(record: &HashMap<String, String>, text: &HashMap<String, String>) -> anyhow::Result<Self> {
        let id: u32 = record.get("id").unwrap().parse()?;
        let name = {
            let key = record.get("name").unwrap();
            text.get(key).unwrap().clone()
        };
        let r#type: Type = {
            let val: u8 = record.get("type").unwrap().parse()?;
            Type::from_u8(val).expect("unknown gun type")
        };
        let rank: u8 = {
            let val: u8 = record.get("rank").unwrap().parse()?;
            if val < 2 || val > 6 {
                panic!("invalid rarity: id={}, name={}", id, name);
            }
            val
        };
        let growth_ratio = StatGrowthRatio {
            hp: record.get("ratio_life").unwrap().parse()?,
            fp: record.get("ratio_pow").unwrap().parse()?,
            rof: record.get("ratio_rate").unwrap().parse()?,
            accuracy: record.get("ratio_hit").unwrap().parse()?,
            evasion: record.get("ratio_dodge").unwrap().parse()?,
            armour: record.get("ratio_armor").unwrap().parse()?,
        };
        let armor_piercing: u16 = record.get("armor_piercing").unwrap().parse()?;
        let crit: u16 = record.get("crit").unwrap().parse()?;
        let special: u16 = record.get("special").unwrap().parse()?;
        let eat_ratio: u16 = record.get("eat_ratio").unwrap().parse()?;

        let effect_grid_center: i8 = record.get("effect_grid_center").unwrap().parse()?;
        let effect_guntype: Vec<Type> = record.get("effect_guntype")
            .unwrap()
            .split(",")
            .map(|t| t.parse().unwrap())
            .filter_map(|t: u8| Type::from_u8(t))
            .collect();
        let effect_grid_pos: Vec<i8> = record
            .get("effect_grid_pos").unwrap()
            .split(",")
            .map(|t| t.parse().unwrap())
            .collect();

        let tiles = FormationBuffTiles::new_from_stc(effect_grid_center, effect_grid_pos);
        let effect_grid_effect = record
            .get("effect_grid_effect").unwrap()
            .split(';')
            .map(|te| {
                let mut te = te.splitn(2, ',');
                let eff: u8 = te.next().unwrap().parse().unwrap();
                let val: f32 = te.next().unwrap().parse().unwrap();
                (eff, val)
            })
            .fold(FormationBuffEffects::default(), |mut fe, (eff, val)| {
                match eff {
                    1 => fe.firepower = val,
                    2 => fe.rof = val,
                    3 => fe.accuracy = val,
                    4 => fe.evasion = val,
                    5 => fe.crit_rate = val,
                    6 => fe.cdr = val,
                    // 7 ap, unused
                    8 => fe.armour = val,
                    _ => panic!()
                }
                fe
            })
            ;

        let type_equip1 = parse_slot_compatible_equipment("type_equip1", record);
        let type_equip2 = parse_slot_compatible_equipment("type_equip2", record);
        let type_equip3 = parse_slot_compatible_equipment("type_equip3", record);

        Ok(DollBase {
            id,
            name,
            r#type,
            rarity: rank,
            growth_ratio: eat_ratio,
            crit_rate: crit,
            armour_pierce: armor_piercing,
            rounds: special,
            stat_growth: growth_ratio,
            formation_buff: FormationBuff { tiles, affects: effect_guntype, effects: effect_grid_effect },
            compatible_equipment: (type_equip1, type_equip2, type_equip3),
        })
    }
}

#[derive(Debug, Clone)]
pub struct Doll {
    // calculation is done using floats, so let's just call everything a float
    pub r#type: Type,
    pub rarity: f32, // 2-6
    pub links: f32,  // 1-5
    pub ammo_rounds: Stat, // for MG and SG
    pub night_vision: Stat, // percentage 0-100%
    pub crit_rate: Stat,    // percentage
    pub crit_dmg: Stat,     // percentage
    pub armour_pierce: Stat,
    pub firepower: Stat,
    pub accuracy: Stat,
    pub evasion: Stat,
    pub rof: Stat,
    pub armour: Stat,
    pub current_hp: f32,
    pub max_hp: f32,
    skill1_lv: f32,
    skill2_lv: f32,
    pub compatible_equipment: ((equipment::Category, Vec<equipment::Type>), (equipment::Category, Vec<equipment::Type>), (equipment::Category, Vec<equipment::Type>)),
    pub equipment: (Option<Equipment>, Option<Equipment>, Option<Equipment>),
    pub fairy: Option<Fairy>,
    pub formation_buff: FormationBuff,
    pub tile_buffs: FormationBuffEffects, // tile buffs that the doll is receiving in percentages
}

macro_rules! stat_getter {
    ($name:ident) => {
        pub fn $name(&self) -> Stat {
            let mut stat = self.$name;

            if let Some(equip) = &self.equipment.0 {
                stat.equipment += equip.$name
            }
            if let Some(equip) = &self.equipment.1 {
                stat.equipment += equip.$name
            }
            if let Some(equip) = &self.equipment.2 {
                stat.equipment += equip.$name
            }

            stat
        }
    };
    ($name:ident, with fairy) => {
        pub fn $name(&self) -> Stat {
            let mut stat = self.$name;

            if let Some(equip) = &self.equipment.0 {
                stat.equipment += equip.$name
            }
            if let Some(equip) = &self.equipment.1 {
                stat.equipment += equip.$name
            }
            if let Some(equip) = &self.equipment.2 {
                stat.equipment += equip.$name
            }

            if let Some(fairy) = &self.fairy {
                stat.fairy += stat.total() / 100.0 * fairy.$name;
            }

            stat
        }
    }
}

impl Doll {
    pub fn new_with_max_stats(base: &DollBase, favour_ratio: f32) -> Self {
        let is_mod = if base.id > 20_000 { true } else { false };
        let level = if is_mod { 120.0 } else { 100.0 };
        Self::new_with_level(base, level, favour_ratio)
    }

    pub fn new_with_level(base: &DollBase, level: f32, favour_ratio: f32) -> Self {
        let r#type = base.r#type.clone();
        let is_mod = if base.id > 20_000 { true } else { false };
        let links = match level {
            _ if level < 10.0 => 1.0,
            _ if level < 30.0 => 2.0,
            _ if level < 70.0 => 3.0,
            _ if level < 90.0 => 4.0,
            _ => 5.0,
        };
        let growth_ratio: f32 = base.growth_ratio.into();
        let stat_growth = &base.stat_growth;

        let (hp_base, fp_growth, rof_growth, accuracy_growth, evasion_growth, armour_base) =
            if is_mod {
                (&HP_BASE_MOD, &FP_GROWTH_MOD, &ROF_GROWTH_MOD, &ACC_GROWTH_MOD, &EVA_GROWTH_MOD, &ARMOUR_BASE_MOD)
            } else {
                (&HP_BASE,     &FP_GROWTH,     &ROF_GROWTH,     &ACC_GROWTH,     &EVA_GROWTH,     &ARMOUR_BASE)
            };

        macro_rules! calculate_stat {
            ($name:ident, $base:expr, $growth:expr, $factor:expr, $stat_growth:expr) => {
                let $name = {
                    let base = ($base[0] * $factor * $stat_growth / $base[1]);
                    let additional = (
                        ($growth[3] + (level - 1.0) * $growth[0])
                            * $factor
                            * $stat_growth
                            * growth_ratio
                            / $growth[1]
                            / $growth[2]
                    );
                    let mut value = Stat { base, additional, ..Default::default() };
                    value.affection = favour_ratio.signum() * f32::abs(value.total() * favour_ratio);
                    
                    value
                };
            };
        }

        calculate_stat!(firepower, FP_BASE, fp_growth, r#type.base_fp_factor(), stat_growth.fp);
        calculate_stat!(accuracy, ACC_BASE, accuracy_growth, r#type.base_accuracy_factor(), stat_growth.accuracy);
        calculate_stat!(evasion, EVA_BASE, evasion_growth, r#type.base_evasion_factor(), stat_growth.evasion);
        let max_hp = f32::ceil(
            (hp_base[0] + (level - 1.0) * hp_base[1]) * r#type.base_hp_factor() * stat_growth.hp
                / hp_base[2],
        ) * links;
        let rof = {
            let base =
                ROF_BASE[0] * r#type.base_rof_factor() * stat_growth.rof / ROF_BASE[1];
            let additional = 
                (rof_growth[3] + (level - 1.0) * rof_growth[0])
                    * r#type.base_rof_factor()
                    * stat_growth.rof
                    * growth_ratio
                    / rof_growth[1]
                    / rof_growth[2]
            ;
            // no affection bonus
            Stat { base, additional, ..Default::default() }
        };
        let armour = {
            let value = (armour_base[0] + (level - 1.0) * armour_base[1])
                * r#type.base_armour_factor()
                * stat_growth.armour
                / armour_base[2];
            Stat { base: value, ..Default::default() }
        };

        Self {
            r#type,
            rarity: base.rarity.into(),
            links,
            ammo_rounds: Stat { base: base.rounds.into(), ..Default::default() },
            night_vision: Stat { base: 0.0, ..Default::default() },
            crit_rate: Stat { base: base.crit_rate.into(), ..Default::default() },
            crit_dmg: Stat { base: 150.0, ..Default::default() },
            armour_pierce: Stat { base: base.armour_pierce.into(), ..Default::default() },
            firepower,
            rof,
            accuracy,
            evasion,
            armour,
            current_hp: max_hp,
            max_hp,
            skill1_lv: 10.0,
            skill2_lv: if is_mod { 10.0 } else { 0.0 },
            compatible_equipment: base.compatible_equipment.clone(),
            equipment: (None, None, None),
            fairy: None,
            formation_buff: base.formation_buff.clone(),
            tile_buffs: FormationBuffEffects::default(),
        }
    }

    stat_getter!(ammo_rounds);
    stat_getter!(night_vision);
    stat_getter!(crit_rate);
    stat_getter!(crit_dmg, with fairy);
    stat_getter!(armour_pierce);
    stat_getter!(firepower, with fairy);
    stat_getter!(accuracy, with fairy);
    stat_getter!(evasion, with fairy);
    stat_getter!(rof);
    stat_getter!(armour, with fairy);

    pub fn ce(&self, is_night: bool, is_full_hp: bool, with_tilebuffs: bool) -> f32 {
        let attack_ce = self.attack_ce(is_night, is_full_hp, with_tilebuffs);
        let defence_ce = self.defense_ce(is_full_hp, with_tilebuffs);
        let skill1_ce = f32::ceil(self.skill1_ce(is_full_hp, with_tilebuffs) * (1.0 + 0.0 / 100.0));
        let skill2_ce = self.skill2_ce(is_full_hp);
        attack_ce + defence_ce + skill1_ce + skill2_ce
    }

    fn attack_ce(&self, is_night: bool, is_full_hp: bool, with_tilebuffs: bool) -> f32 {
        let mut crit_rate = self.crit_rate().total();
        let crit_dmg = self.crit_dmg().total() - 100.0;
        let mut firepower = self.firepower().total();
        let mut accuracy = self.accuracy().total();
        let mut rof = self.rof().total();
        let ammo_rounds = self.ammo_rounds().total();
        let armour_pierce = self.armour_pierce.total();
        let night_vision = self.night_vision().total();

        if with_tilebuffs {
            crit_rate = f32::ceil(f32::max(crit_rate * (1.0 + self.tile_buffs.crit_rate / 100.0), 0.0));
            firepower = f32::ceil(f32::max(firepower * (1.0 + self.tile_buffs.firepower / 100.0), 0.0));
            accuracy = f32::ceil(f32::max(accuracy * (1.0 + self.tile_buffs.accuracy / 100.0), 1.0));
            rof = f32::ceil(f32::max(rof * (1.0 + self.tile_buffs.rof / 100.0), 1.0));
        }

        let hp = if is_full_hp {
            self.max_hp
        } else {
            self.current_hp
        };

        if is_night {
            const NIGHT_DEBUFF: f32 = -0.9;
            accuracy = f32::ceil(accuracy * (1.0 + NIGHT_DEBUFF * (1.0 - night_vision)));
        }

        match self.r#type {
            Type::SG => {
                const ATTACK_EFFECT_SG: [f32; 9] = [6.0, 1.5, 23.0, 8.0, 50.0, 3.0, 3.0, 1.5, 0.5];
               
                f32::ceil(
                    ATTACK_EFFECT_SG[0]
                    * f32::ceil(hp / self.max_hp * self.links)
                    * (
                        ATTACK_EFFECT_SG[6]
                        * ammo_rounds
                        * (firepower + armour_pierce / ATTACK_EFFECT_SG[5])
                        * (crit_dmg / 100.0 * crit_rate / 100.0 + 1.0)
                        / (ammo_rounds * ATTACK_EFFECT_SG[4] / rof + ATTACK_EFFECT_SG[7] + ATTACK_EFFECT_SG[8] * ammo_rounds)
                        * accuracy
                        / (accuracy + ATTACK_EFFECT_SG[2])
                        + ATTACK_EFFECT_SG[3]
                    )
                )                
            },
            Type::MG => {
                const ATTACK_EFFECT_MG: [f32; 7] = [7.0, 1.5, 4.0, 200.0, 23.0, 8.0, 3.0];

                f32::ceil(
                    ATTACK_EFFECT_MG[0]
                    * f32::ceil(hp / self.max_hp * self.links)
                    * (
                        ammo_rounds
                        * (firepower + armour_pierce / ATTACK_EFFECT_MG[6])
                        * (crit_dmg / 100.0 * crit_rate / 100.0 + 1.0)
                        / (ammo_rounds / 3.0 + ATTACK_EFFECT_MG[2] + ATTACK_EFFECT_MG[3] / rof)
                        * accuracy
                        / (accuracy + ATTACK_EFFECT_MG[4])
                        + ATTACK_EFFECT_MG[5]
                    )
                )
            },
            _ => {
                const ATTACK_EFFECT_NORMAL: [f32; 6] = [5.0, 1.5, 23.0, 8.0, 50.0, 3.0];

                f32::ceil(
                    ATTACK_EFFECT_NORMAL[0]
                    * f32::ceil(hp / self.max_hp * self.links)
                    * (
                        (firepower + armour_pierce / ATTACK_EFFECT_NORMAL[5])
                        * (crit_dmg / 100.0 * crit_rate / 100.0 + 1.0)
                        * rof
                        / ATTACK_EFFECT_NORMAL[4]
                        * accuracy
                        / (accuracy + ATTACK_EFFECT_NORMAL[2])
                        + ATTACK_EFFECT_NORMAL[3]
                    )
                )
            }
        }
    }

    fn defense_ce(&self, is_full_hp: bool, with_tilebuffs: bool) -> f32 {
        let mut evasion = self.evasion().total();
        let mut armour = self.armour().total();

        if with_tilebuffs {
            evasion = f32::ceil(f32::max(evasion * (1.0 + self.tile_buffs.evasion / 100.0), 0.0));
            armour = f32::ceil(f32::max(armour * (1.0 + self.tile_buffs.armour / 100.0), 0.0));
        }

        let hp = if is_full_hp {
            self.max_hp
        } else {
            self.current_hp
        };
        
        const DEFENCE_EFFECT: [f32; 6] = [1.0, 35.0, 4.2, 100.0, 3.2, 1.0];

        f32::floor(
            DEFENCE_EFFECT[0] 
            * f32::ceil(
                hp
                * ((DEFENCE_EFFECT[1] + evasion) / DEFENCE_EFFECT[1])
                * (
                    DEFENCE_EFFECT[2]
                    * DEFENCE_EFFECT[3]
                    / f32::max(DEFENCE_EFFECT[3] - armour / DEFENCE_EFFECT[5], 1.0)
                    - DEFENCE_EFFECT[4]
                )
            )
        )
    }

    fn skill1_ce(&self, is_full_hp: bool, with_tilebuffs: bool) -> f32 {
        if self.skill1_lv == 0.0 { return 0.0 } // SL1 is always at least 1 for player dolls, but JIC

        let cdr = if with_tilebuffs {
            1.0 + self.tile_buffs.cdr / 100.0
        } else {
            1.0
        };

        let hp = if is_full_hp {
            self.max_hp
        } else {
            self.current_hp
        };

        const SKILL1_EFFECT: [f32; 2] = [35.0, 5.0];

        f32::ceil(
            f32::ceil(hp / self.max_hp * self.links)
            * (0.8 + self.rarity / 10.0)
            * (SKILL1_EFFECT[0] + SKILL1_EFFECT[1] * (self.skill1_lv - 1.0))
            // * f32::ceil(self.skill1_lv / 10.0) // unnecessary if we check for SL0 and valid levels are 0-10
            * cdr
        )
    }

    fn skill2_ce(&self, is_full_hp: bool) -> f32 {
        if self.skill2_lv == 0.0 { return 0.0 }

        // not affected by CDR

        let hp = if is_full_hp {
            self.max_hp
        } else {
            self.current_hp
        };

        const SKILL2_EFFECT: [f32; 2] = [15.0, 2.0];

        f32::ceil(
            f32::ceil(hp / self.max_hp * self.links)
            * (0.8 + self.rarity / 10.0)
            * (SKILL2_EFFECT[0] + SKILL2_EFFECT[1] * (self.skill2_lv - 1.0))
            // * f32::ceil(self.skill2_lv / 10.0) // unnecessary if we check for SL0 and valid levels are 0-10
        )
    }
}

impl fmt::Display for Doll {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Doll {{ type: {:?}, rarity: {}*, links: {}",
            self.r#type, self.rarity, self.links,
        )?;
        if !self.ammo_rounds.is_zero()   { write!(f, ", ammo: {}", self.ammo_rounds)? }
        if !self.night_vision.is_zero()  { write!(f, ", night vision: {}", self.night_vision)? }
        if !self.crit_rate.is_zero()     { write!(f, ", crit rate: {}", self.crit_rate)? }
        if !self.crit_dmg.is_zero()      { write!(f, ", crit dmg: {}", self.crit_dmg)? }
        if !self.armour_pierce.is_zero() { write!(f, ", ap: {}", self.armour_pierce)? }
        if !self.firepower.is_zero()     { write!(f, ", fp: {}", self.firepower)? }
        if !self.accuracy.is_zero()      { write!(f, ", acc: {}", self.accuracy)? }
        if !self.evasion.is_zero()       { write!(f, ", eva: {}", self.evasion)? }
        if !self.rof.is_zero()           { write!(f, ", rof: {}", self.rof)? }
        if !self.armour.is_zero()        { write!(f, ", armour: {}", self.armour)? }
        write!(f, ", hp: {}", self.max_hp)?;
        write!(f, " }}")
    }
}

pub fn load_gun_data<P>(stc: P, text: P) -> anyhow::Result<Vec<DollBase>>
where
    P: AsRef<Path>,
{
    let text: HashMap<String, String> = {
        let contents = fs::read(text).context("failed to read text data")?;
        serde_json::from_slice(&contents).context("failed to deserialize text data")?
    };

    let dolls = csv::Reader::from_path(stc)
        .context("failed to open csv parser")?
        .deserialize()
        .skip(1) // header already consumed by opening, step over types
        .filter_map(Result::ok)
        .filter(|r: &HashMap<String, String>| {
            let id: u32 = r
                .get("id")
                .expect("failed to get id")
                .parse()
                .expect("failed to convert id");
            !(id > 9000 && id < 20000)
        })
        .map(|r| DollBase::from_stc(&r, &text).expect("failed to serialize doll record"))
        .collect();

    Ok(dolls)
}

#[test]
fn test() {
    use equipment::Category::Accessory;
    use equipment::Type::OpticalSight;

    // ar15 mod3 ?
    {  
        let doll = Doll {
            r#type: Type::AR,
            rarity: 5.0,
            links: 5.0,
            ammo_rounds: Stat { base: 0.0, ..Default::default() },
            night_vision: Stat { base: 0.0, ..Default::default() },
            crit_rate: Stat { base: 30.0, equipment: 48.0, ..Default::default() },
            crit_dmg: Stat { base: 150.0, ..Default::default() },
            armour_pierce: Stat { base: 15.0, ..Default::default() },
            firepower: Stat { base: 55.0, equipment: 29.0, ..Default::default() },
            accuracy: Stat { base: 53.0, equipment: 9.0, ..Default::default() },
            evasion: Stat { base: 49.0, ..Default::default() },
            rof: Stat { base: 79.0, equipment: -2.0, ..Default::default() },
            armour: Stat { base: 0.0, ..Default::default() },
            current_hp: 565.0,
            max_hp: 565.0,
            skill1_lv: 10.0,
            skill2_lv: 10.0,
            compatible_equipment: ((Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight])),
            equipment: (None, None, None),
            fairy: None,
            formation_buff: FormationBuff::default(),
            tile_buffs: FormationBuffEffects::default(),
        };
    
        let is_night = false;
        let is_full_hp = true;
        let with_tilebuffs = false;
    
        let attack_ce = doll.attack_ce(is_night, is_full_hp, with_tilebuffs);
        let defence_ce = doll.defense_ce(is_full_hp, with_tilebuffs);
        let skill1_ce = doll.skill1_ce(is_full_hp, with_tilebuffs);
        let skill2_ce = doll.skill2_ce(is_full_hp);
        let ce = attack_ce + defence_ce + skill1_ce + skill2_ce;

        assert_eq!(attack_ce, 3675.0);
        assert_eq!(defence_ce, 1356.0);
        assert_eq!(skill1_ce, 520.0);
        assert_eq!(skill2_ce, 215.0);
        assert_eq!(ce, 5766.0);
    }

    // serdyukov
    {
        let doll = Doll {
            r#type: Type::HG,
            rarity: 3.0,
            links: 5.0,
            ammo_rounds: Stat { base: 0.0, ..Default::default() },
            night_vision: Stat { base: 0.0, ..Default::default() },
            crit_rate: Stat { base: 20.0, equipment: 13.0, ..Default::default() },
            crit_dmg: Stat { base: 150.0, ..Default::default() },
            armour_pierce: Stat { base: 15.0, equipment: -5.0, ..Default::default() },
            firepower: Stat { base: 35.0, equipment: -1.0, ..Default::default() },
            accuracy: Stat { base: 61.0, ..Default::default() },
            evasion: Stat { base: 72.0, equipment: 42.0, ..Default::default() },
            rof: Stat { base: 58.0, ..Default::default() },
            armour: Stat { base: 0.0, ..Default::default() },
            current_hp: 207.0,
            max_hp: 345.0,
            skill1_lv: 6.0,
            skill2_lv: 0.0,
            compatible_equipment: ((Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight])),
            equipment: (None, None, None),
            fairy: None,
            formation_buff: FormationBuff::default(),
            tile_buffs: FormationBuffEffects::default(),
        };

        let is_night = false;
        let is_full_hp = true;
        let with_tilebuffs = false;
        
        assert_eq!(1116.0, doll.attack_ce(is_night, is_full_hp, with_tilebuffs));
        assert_eq!(1469.0, doll.defense_ce(is_full_hp, with_tilebuffs));
        assert_eq!(330.0, doll.skill1_ce(is_full_hp, with_tilebuffs));
        assert_eq!(0.0, doll.skill2_ce(is_full_hp));
        assert_eq!(2915.0, doll.ce(is_night, is_full_hp, with_tilebuffs));

        let is_full_hp = false;

        assert_eq!(670.0, doll.attack_ce(is_night, is_full_hp, with_tilebuffs));
        assert_eq!(882.0, doll.defense_ce(is_full_hp, with_tilebuffs));
        assert_eq!(199.0, doll.skill1_ce(is_full_hp, with_tilebuffs));
        assert_eq!(0.0, doll.skill2_ce(is_full_hp));
        assert_eq!(1751.0, doll.ce(is_night, is_full_hp, with_tilebuffs));
    }

    // pkp 
    {
        let doll = Doll {
            r#type: Type::MG,
            rarity: 5.0,
            links: 5.0,
            ammo_rounds: Stat { base: 10.0, ..Default::default() },
            night_vision: Stat { base: 0.0, ..Default::default() },
            crit_rate: Stat { base: 5.0, ..Default::default() },
            crit_dmg: Stat { base: 150.0, ..Default::default() },
            armour_pierce: Stat { base: 15.0, ..Default::default() },
            firepower: Stat { base: 100.0, ..Default::default() },
            accuracy: Stat { base: 33.0, ..Default::default() },
            evasion: Stat { base: 31.0, ..Default::default() },
            rof: Stat { base: 127.0, ..Default::default() },
            armour: Stat { base: 0.0, ..Default::default() },
            current_hp: 825.0,
            max_hp: 825.0,
            skill1_lv: 10.0,
            skill2_lv: 0.0,
            compatible_equipment: ((Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight])),
            equipment: (None, None, None),
            fairy: None,
            formation_buff: FormationBuff::default(),
            tile_buffs: FormationBuffEffects::default(),
        };

        let is_night = false;
        let is_full_hp = true;
        let with_tilebuffs = false;
        
        assert_eq!(4848.0, doll.ce(is_night, is_full_hp, with_tilebuffs));
    }

    // sat8
    {
        let doll = Doll {
            r#type: Type::SG,
            rarity: 5.0,
            links: 5.0,
            ammo_rounds: Stat { base: 4.0, ..Default::default() },
            night_vision: Stat { base: 0.0, ..Default::default() },
            crit_rate: Stat { base: 40.0, ..Default::default() },
            crit_dmg: Stat { base: 150.0, ..Default::default() },
            armour_pierce: Stat { base: 15.0, ..Default::default() },
            firepower: Stat { base: 31.0, ..Default::default() },
            accuracy: Stat { base: 14.0, ..Default::default() },
            evasion: Stat { base: 13.0, ..Default::default() },
            rof: Stat { base: 33.0, ..Default::default() },
            armour: Stat { base: 22.0, ..Default::default() },
            current_hp: 1410.0,
            max_hp: 1410.0,
            skill1_lv: 10.0,
            skill2_lv: 0.0,
            compatible_equipment: ((Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight]), (Accessory, vec![OpticalSight])),
            equipment: (None, None, None),
            fairy: None,
            formation_buff: FormationBuff::default(),
            tile_buffs: FormationBuffEffects::default(),
        };

        let is_night = false;
        let is_full_hp = true;
        let with_tilebuffs = false;

        let attack_ce = doll.attack_ce(is_night, is_full_hp, with_tilebuffs);
        let defence_ce = doll.defense_ce(is_full_hp, with_tilebuffs);
        let skill1_ce = doll.skill1_ce(is_full_hp, with_tilebuffs);
        let skill2_ce = doll.skill2_ce(is_full_hp);

        assert_eq!(856.0, attack_ce);
        assert_eq!(4225.0, defence_ce);
        assert_eq!(520.0, skill1_ce);
        assert_eq!(0.0, skill2_ce);
        assert_eq!(5601.0, doll.ce(is_night, is_full_hp, with_tilebuffs));
    }
}

fn parse_slot_compatible_equipment(key: &str, record: &HashMap<String, String>) -> (equipment::Category, Vec<equipment::Type>) {
    // category;type,type
    let mut val = record.get(key).unwrap().splitn(2, ";");

    let category: u8 = val.next().unwrap().parse().unwrap();
    let category = equipment::Category::from_u8(category).unwrap();

    let types: Vec<equipment::Type> = val
        .next()
        .unwrap()
        .split(",").map(|t| equipment::Type::from_u8(t.parse().unwrap()).unwrap())
        .collect();
    (category, types)
}

#[test]
fn sf() {
    let is_full_hp = true;
    
    let links = 1.0;
    let rarity = 5.0;
    let max_hp = 1091.0;
    let current_hp = max_hp;
    let crit_rate = 20.0;
    let crit_dmg = 50.0;
    let armour_pierce = 15.0;
    let firepower = 624.0;
    let accuracy = 102.0;
    let evasion = 209.0;
    let rof = 126.0;
    let armour = 0.0;
    let skill1_lv = 10.0;
    let skill2_lv = 10.0;
    let talent1_lv = 1.0;
    let talent2_lv = 1.0;

    const ATTACK_EFFECT_NORMAL: [f32; 6] = [5.0,  0.01, 23.0, 8.0,   50.0, 3.0];
    const DEFENCE_EFFECT:       [f32; 6] = [1.0,  35.0, 2.6,  200.0, 1.6,  1.0];
    const SKILL1_EFFECT:        [f32; 2] = [35.0, 5.0];
    const SKILL2_EFFECT:        [f32; 2] = [15.0, 2.0];
    const ADVANCE_EFFECT:       [f32; 2] = [15.0, 2.0];
    const TALENT_EFFECT:        [f32; 2] = [15.0, 2.0];
    
    let hp = if is_full_hp { max_hp } else { current_hp };

    let attack_ce = f32::ceil(
        ATTACK_EFFECT_NORMAL[0]
        * f32::ceil(hp / max_hp * links)
        * (
            (firepower + armour_pierce / ATTACK_EFFECT_NORMAL[5])
            * (crit_dmg / 100.0 * crit_rate / 100.0 + 1.0)
            * rof
            / ATTACK_EFFECT_NORMAL[4]
            * accuracy
            / (accuracy + ATTACK_EFFECT_NORMAL[2])
            + ATTACK_EFFECT_NORMAL[3]
        )
    );
    
    let defence_ce = f32::floor(
        DEFENCE_EFFECT[0] 
        * f32::ceil(
            hp
            * ((DEFENCE_EFFECT[1] + evasion) / DEFENCE_EFFECT[1])
            * (
                DEFENCE_EFFECT[2]
                * DEFENCE_EFFECT[3]
                / f32::max(DEFENCE_EFFECT[3] - armour / DEFENCE_EFFECT[5], 1.0)
                - DEFENCE_EFFECT[4]
            )
        )
    );

    let skill1_ce = f32::ceil(
            f32::ceil(hp / max_hp * links)
            * (0.8 + rarity / 10.0)
            * (SKILL1_EFFECT[0] + SKILL1_EFFECT[1] * (skill1_lv - 1.0))
            * f32::ceil(skill1_lv / 10.0)
    );

    let skill2_ce = f32::ceil(
        f32::ceil(hp / max_hp * links)
        * (0.8 + rarity / 10.0)
        * (SKILL2_EFFECT[0] + SKILL2_EFFECT[1] * (skill2_lv - 1.0))
        * f32::ceil(skill2_lv / 10.0)
    );

    let advance_ce = f32::ceil(
        f32::ceil(hp / max_hp * links) *
        (0.8 + rarity / 10.0)
        * (ADVANCE_EFFECT[0] + ADVANCE_EFFECT[1] * (talent1_lv - 1.0))
        * f32::ceil(talent1_lv / 10.0)
    );

    let talent_ce = f32::ceil(
        f32::ceil(hp / max_hp * links) *
        (0.8 + rarity / 10.0)
        * (TALENT_EFFECT[0] + TALENT_EFFECT[1] * (talent2_lv - 1.0))
        * f32::ceil(talent2_lv / 10.0)
    );

    let ce = attack_ce + defence_ce + skill1_ce + skill2_ce + advance_ce + talent_ce;
    println!(" attack_ce={}", attack_ce);
    println!("defence_ce={}", defence_ce);
    println!(" skill1_ce={}", skill1_ce);
    println!(" skill2_ce={}", skill2_ce);
    println!("advance_ce={}", advance_ce);
    println!(" talent_ce={}", talent_ce);
    println!("        ce={}", ce);

    assert_eq!(ce, 14940.0);
}